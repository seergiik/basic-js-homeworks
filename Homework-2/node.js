let username = "Sergei";
let password = "password";

let userEnteredPassword = prompt("Please enter your password:");

if (userEnteredPassword === password) {
    console.log("Password is correct");
} else {
    console.log("Password is incorrect");
}

let x = 5;
let y = 3;

let sum = x + y;
let difference = x - y;
let product = x * y;
let quotient = x / y;

alert("Addition of x and y: " + sum +
    "\nSubtraction of x and y: " + difference +
    "\nMultiplication of x and y: " + product +
    "\nDivision of x by y: " + quotient);