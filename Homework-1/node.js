// Задача 1: 
let numberVariable = 42; 
console.log(typeof numberVariable === 'number');

// Задача 2: 
let name = 'Sergei'; 
let lastName = 'Babushkin'; 
console.log(`My name is ${name}, and my last name is ${lastName}`); 

// Задача 3: 
let numericVariable = 10; 
console.log(`The number is: ${numericVariable}`); 